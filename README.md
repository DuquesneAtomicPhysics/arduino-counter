# Arduino UNO frequency counter
Simple Arduino sketch implementing a frequency counter
by Ted Corcovilos 2019-06-05

## Instructions
Wire the input to pin 5.  This should be a TTL digital signal.

Serial output (default baud rate of 115200) will display the measured frequency in Hertz, sampled every 8 milliseconds.

The useful input range of frequencies is a few hundred Hz to a theoretical maximum of 8 MHz (Nyquist limit).

## How it works
Timer1 is set to external clock mode and increments on the rising edge of Pin 5.
Timer2 is set up to fire an interrupt every 8.192 ms.
When this happens, the code records the value of Timer1 and resets it.
The recorded value is converted to frequency and displayed on the Serial out.

To enable some additional debugging output to the Serial line, turn on the `DEBUG` flag at the top of the code.

## Known issues
* The circuit undercounts, especially at high frequencies (> 2 MHz).
This is partially because it only counts on the rising edge, so it may miss a partial pulse at the beginning or end of the sample window.
Also, the pulse shape starts to get corrupted above about 1 MHz which can cause pulses to be missed.
This effect can be mitigated with good wiring.

* The code only runs on Arduino UNO compatible boards because it addresses the timer registers directly.