/*****************

   Pulse counter
   by Ted Corcovilos
   20190530
   Counts TTL pulses on D5 input and reports a value every ~0.01 s
   Assumes atmega328p hardware pins (Arduino UNO and compatible)
   Input signal connected to pin 5 (Timer1)
*/

#undef DEBUG // set to enable some debugging statements in the Serial output

volatile uint16_t count;
volatile bool newcountflag;
volatile bool overflowflag;
const double CLOCKFREQ = 16000000;
const uint16_t PRESCALE = 1024;
const uint8_t TRIGGER = 128;

#define BAUD 115200 // Baud rate for the serial port

void setup() {
  pinMode(5, INPUT_PULLUP); // Pin 5 is the one connected to Timer1

  noInterrupts(); // put things on freeze while setting up the timers

  /* Use Timer2 to interrupt every 8.192 ms
    Do this by setting the prescale to 1024 and comparison value to 128
    (Assuming 16 MHz clock).
    These values can be adjusted in the constants at the top of the file
  */
  TCCR2A = 0; // clear timer2 control register
  TCCR2B = 0;
  TCCR2A |= (1 << WGM21); // turn on count to clear mode
  TCCR2B |= (1 << CS20) | (1 << CS21) | (1 << CS22); // set prescaler to 1024
  TIMSK2 |= (1 << OCIE2A); // set count interrupt
  OCR2A = TRIGGER - 1; // Set limit value on Timer 2 (minus one for reset)

  /* Use Timer1 as the counter */
  TCCR1A = 0;
  TCCR1B = 0;
  TIMSK1 = 0; // No interrupts on Timer 1
  TCCR1B |= (1 << CS10) | (1 << CS11) | (1 << CS12); // count external rising edge

  interrupts(); // start everything

  Serial.begin(BAUD); // initialize the serial monitor

  count = 0;
  newcountflag = false;
  overflowflag = false;
}

ISR(TIMER2_COMPA_vect)
{
  // Interrupt handler for Timer2 (clock)
  // TODO fill this
  count = TCNT1; // read counter
  overflowflag = TIFR1 & B00000001; // did we overflow?
  TCNT1 = 0; // reset counter
  TIFR1 = 0; // reset counter flags
  newcountflag = true;
}

void loop() {
  uint16_t lastcount;
  double freq;
  if (newcountflag) {
  noInterrupts();
    lastcount = count;
    newcountflag = false;
    interrupts();

    if (overflowflag)
  {
    freq = -1; // report overflow as a negative reading
  }
  else
  {
    freq = 1.0*lastcount * CLOCKFREQ / PRESCALE / TRIGGER;
  }
#ifdef DEBUG
  Serial.print("Time: ");
  Serial.print(millis());
  Serial.print("\tCounts per interval: ");
  Serial.print(lastcount);
  Serial.print("\tFreq. (Hz): ");
#endif
  Serial.println(freq);
  }
}
